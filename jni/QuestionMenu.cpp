#include "QuestionMenu.hpp"
extern bool music, soundEffects;
extern Vector2f factor;


sf::String fromUtf8(const string &in)
{
    basic_string<sf::Uint32> tmp;
    sf::Utf8::toUtf32(in.begin(), in.end(), back_inserter(tmp));
    return sf::String(tmp);
}

/////////////////////////////////////////// Constructor
#include <stdlib.h>
#include "sqlite/sqlite3.h"
#include <string>
using namespace std;
QuestionMenu::QuestionMenu (Game *game, Horse *cpuHorse, Horse *playerHorse, std::vector <Subject> &subjects)
                           : Scene (game), cpuHorse (cpuHorse), playerHorse (playerHorse)
{
	auto & font = ResourceManager::getFont (ASSETS"fonts/RawengulkSans-094.ttf");
	
	this->subjects = &subjects;
	
	spriteBackground.setTexture (ResourceManager::getTexture (ASSETS"backgrounds/paper.png"));
	spriteBackground.setScale (factor);
	
	Text * button = new Text ("Revisar", font, 60U);
	button->setFillColor (Color::Black);
	
	readyBtn.create (new Sprite (ResourceManager::getTexture (ASSETS"buttons/gameplay.png"),IntRect (0.f, 156.f, 293.f, 91.f)), 
	                             button,Vector2f (1100.f * factor.x, 700.f *factor.y), &game->window );
	readyBtn.scale (factor);
	
	underlined.setTexture (ResourceManager::getTexture (ASSETS"underlined.png"));
	underlined.scale (factor);
	setOriginCenter (underlined);
	
	///////////////////////////////////////// Question and Answers
	sqlite3 *database;
	sqlite3_stmt *statement;

	#ifdef BYS_DESKTOP
	if (sqlite3_open (ASSETS"files/marathon", &database) == SQLITE_OK)
	#elif defined BYS_MOBILE
	if (sqlite3_open ("/data/data/com.bysgames.marathon/files/marathon.db", &database) == SQLITE_OK)
	#endif
	{
		randomSubject = rand () % subjects.size ();
		difficulty = playerHorse->getDifficulty ();
		
		auto & y = subjects [randomSubject];
		auto x = y.id;
		
		string query ("SELECT count (*) FROM Question WHERE SubjectID ='");
		query.append (x.c_str ());
		query.append ("' and State = 2 and Difficulty = ");
		query.append (to_string (difficulty).c_str ());
		query.append (";");
		
		sqlite3_prepare (database, query.c_str (), 1000, &statement, 0);
		sqlite3_step (statement);
		int totalQuestions = sqlite3_column_int (statement, 0);
		sqlite3_finalize (statement);
		
		query.replace (6, 10, " *");
		sqlite3_prepare (database, query.c_str (), 1000, &statement, 0);
		
		int randomQuestion = random () % totalQuestions + 1;
		for (int i = 0; i < randomQuestion; ++i)
			sqlite3_step (statement);

		question.setString (fromUtf8 ((char *) sqlite3_column_text (statement, 1)));
		question.setFont (font);
		question.setCharacterSize (40U);
		setOriginCenter (question);
		question.setPosition (683.f * factor.x, 130.f * factor.y);
		question.setFillColor (Color::Black);
		
		positions [0] = Vector2f (370.f * factor.x, 300.f * factor.y);
		positions [1] = Vector2f (370.f * factor.x, 600.f * factor.y);
		positions [2] = Vector2f (870.f * factor.x, 300.f * factor.y);
		positions [3] = Vector2f (870.f * factor.x, 600.f * factor.y);
		
		int randomQuestionPosition = rand () % 4;
		int j = 3;
		
		for (int i = 0; i < 4; ++i)
		{
			if ( i == randomQuestionPosition)
			{	
				Text * answer = new Text ((fromUtf8 ((char *) sqlite3_column_text (statement, 2))), font, 60U);
				answer->setFillColor (Color::Black);
				answers [i].create (answer, positions [i],  &game->window, true);
				correctAnswer = &answers [i];
			}
			else
			{
				Text * answer = new Text ((fromUtf8 ((char *) sqlite3_column_text(statement, j++))), font, 60U);
				answer->setFillColor (Color::Black);
				answers [i].create (answer, positions [i],  &game->window, true);
			}
		}
		answers [0].changeState ();
		underlined.setPosition (positions [0].x, positions [0].y + answers [0].getBounds ().height);

		query = "";
		query.append ("update Question set State =1 where ID =");
		query.append (to_string (sqlite3_column_int (statement, 0)).c_str ());
		query.append (";");
		sqlite3_finalize (statement);
		sqlite3_prepare (database, query.c_str (), 1000, &statement, 0);
		sqlite3_step (statement);
		sqlite3_finalize (statement);

		subjects [randomSubject].totalOfQuestions ++;

		for (int i = 0; i < 4; i++)
			answersBG.addButton (&answers [i]);
	
		////////////////////////////////////////// Connections
		using namespace std;
		Vector2f normalScale (1.f * factor.x, 1.f * factor.y), onMouseScale (1.2f * factor.x, 1.2f * factor.y);
		#ifdef BYS_DESKTOP	
		readyBtn.onMouseEntered.connect (bind (&Button::scale, &readyBtn, onMouseScale));
		readyBtn.onMouseLeave.connect (bind (&Button::scale, &readyBtn, normalScale));
	
		#elif defined BYS_MOBILE
	
		readyBtn.onMousePressed.connect (bind (&Button::scale, &readyBtn, onMouseScale));
		readyBtn.onMouseReleased.connect (bind (&Button::scale, &readyBtn, normalScale));
		
		#endif // BYS_DESKTOP
		
		answersBG.checkedButtonChanged.connect (bind (&QuestionMenu::changeTextureQuestionsBtns, this, placeholders::_1));
		
		readyBtn.onClick.connect (bind (&QuestionMenu::checkAnswer, this));
	}
	
	sqlite3_close (database);
	
	view = &game->window.getDefaultView ();
	game->window.setView (*view);
}

/////////////////////////////////////////// Destroyer
QuestionMenu::~QuestionMenu () {}

/////////////////////////////////////////// Run, Events and Draw
GameTask * QuestionMenu::run () 
{
	while (running)
	{
		while (game->window.pollEvent (this->event))
		{
			switch (event.type)
			{
				#ifdef BYS_DESKTOP
				
				case Event::MouseMoved:
					(readyBtn.*readyBtn.mouseEvent) (event);
					(answers [0].*answers [0].mouseEvent) (event);
					(answers [1].*answers [1].mouseEvent) (event);
					(answers [2].*answers [2].mouseEvent) (event);
					(answers [3].*answers [3].mouseEvent) (event);
				break;
				
				case Event::MouseButtonPressed:
					(readyBtn.*readyBtn.clickEvent) (event);
					(answers [0].*answers [0].clickEvent) (event);
					(answers [1].*answers [1].clickEvent) (event);
					(answers [2].*answers [2].clickEvent) (event);
					(answers [3].*answers [3].clickEvent) (event);
				break;
				
				case Event::MouseButtonReleased:
					(readyBtn.*readyBtn.clickEvent) (event);
					(answers [0].*answers [0].clickEvent) (event);
					(answers [1].*answers [1].clickEvent) (event);
					(answers [2].*answers [2].clickEvent) (event);
					(answers [3].*answers [3].clickEvent) (event);
					
				#elif defined BYS_MOBILE
				
				case Event::TouchBegan:
					(readyBtn.*readyBtn.clickEvent) (event);
					(answers [0].*answers [0].clickEvent) (event);
					(answers [1].*answers [1].clickEvent) (event);
					(answers [2].*answers [2].clickEvent) (event);
					(answers [3].*answers [3].clickEvent) (event);
				break;
				
				case Event::TouchEnded:
					(readyBtn.*readyBtn.clickEvent) (event);
					(answers [0].*answers [0].clickEvent) (event);
					(answers [1].*answers [1].clickEvent) (event);
					(answers [2].*answers [2].clickEvent) (event);
					(answers [3].*answers [3].clickEvent) (event);
					
				#endif //BYS_DESKTOP
				
			}			
		}
		
		game->window.clear ();
		game->window.setView (*view);
		game->window.draw (spriteBackground);
		game->window.draw (question);
		readyBtn.draw ();
		
		for (int i = 0; i < 4; i++)
			answers [i].draw ();
			
		game->window.draw (underlined);
		game->window.display ();		
	}
	
	running = true;
	return task;	
}
	
////////////////////////////////////////// Underlined
void QuestionMenu::changeTextureQuestionsBtns (AbstractButton *abstractButton)
{
	auto bounds = abstractButton->getBounds ();
	underlined.setPosition (bounds.left + bounds.width * 0.5f, bounds.top + bounds.height * 1.5f);	
}

////////////////////////////////////////// Check answer 
void QuestionMenu::checkAnswer () 
{
		if (correctAnswer == answersBG.getCheckedButton ())
		{
			(*subjects) [randomSubject].correctAnswers ++;
			playerHorse->movePositions (difficulty);		
		}
			
		else 
			cpuHorse->movePositions (difficulty);
			
		this->close ();
		task->setType (GameTask::RemoveScene);
}
