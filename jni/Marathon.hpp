#ifndef MARATHON_HPP
#define MARATHON_HPP

#include <SFML/System/Vector2.hpp>
using namespace sf;

#include <BYS/Core/OS.hpp>
using namespace bys;

#ifdef BYS_DESKTOP
	#include "Assets.h"
#elif defined BYS_MOBILE
	#define ASSETS
#endif

namespace marathon
{
	enum MarathonScenes {MainScene, AboutScene, InstructionsScene, GameOverScene, StableScene, SubjectsScene, HipodromeScene, QuestionScene,
					     CreditsScene};
	enum HorseType {Pony, Mustang, Purasangre, CPU};
}

#endif //  MARATHON_HPP 
