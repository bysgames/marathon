#include "MainMenu.hpp"
extern bool music, soundEffects;
extern Vector2f factor;
extern Sound bubbleSound;

///////////////////////////////////////////
#include <SFML/Audio/Sound.hpp>
MainMenu::MainMenu (Game *game) : Scene (game)
{
    auto & buttonsTexture = ResourceManager::getTexture (ASSETS"buttons/buttons_mainMenu.png");
    auto & font = ResourceManager::getFont (ASSETS"fonts/Right Chalk.ttf");
    auto & backgroundMusic = ResourceManager::getMusic (ASSETS"sounds/bensound-littleidea.ogg");
	
	backgroundMusic.play ();
	backgroundMusic.setLoop (music);	
	
	spriteBackground = new Sprite (ResourceManager::getTexture (ASSETS"backgrounds/blackboard.png"));
	spriteBackground->setScale (factor);

	gameTitle.setString ("MARATHON");
	gameTitle.setFont (font);
	gameTitle.setCharacterSize (100U);
	gameTitle.setScale (factor);
	gameTitle.setFillColor (Color (162, 201, 244, 255));
	setOriginCenter (gameTitle);
	gameTitle.setPosition (683.f * factor.x, 100.f * factor.y);

    playBtn.create (new Sprite (buttonsTexture, IntRect (0, 110, 409, 142)), new Text ("JUGAR", font, 60U),
    				Vector2f (683.f * factor.x, 384.f * factor.y), &game->window);
	playBtn.scale (factor);
    aboutBtn.create (new Sprite (buttonsTexture, IntRect (0, 0, 129, 94)), Vector2f (120.f * factor.x, 660.f * factor.y), &game->window);
    aboutBtn.scale (factor);
    profileBtn.create (new Sprite (buttonsTexture, IntRect (129, 0, 100, 100)), Vector2f (120.f * factor.x, 150.f * factor.y), &game->window);
    profileBtn.scale (factor);
    musicBtn.create (new Sprite (buttonsTexture, IntRect (229, 0, 100, 100)), Vector2f (530.f * factor.x, 660.f * factor.y), &game->window, 						true);
	musicBtn.scale (factor);
    soundsBtn.create (new Sprite (buttonsTexture, IntRect (429, 0, 100, 100)), Vector2f (840.f * factor.x, 660.f * factor.y), &game->window, 						true);
    soundsBtn.scale (factor);
    quitBtn.create (new Sprite (buttonsTexture, IntRect (329, 0, 100, 100)), Vector2f (1266.f * factor.x, 660.f * factor.y), &game->window);
    quitBtn.scale (factor);

						    	/////////////////////////////// Conections
	using namespace std;
	Vector2f normalScale (1.f * factor.x, 1.f * factor.y), onMouseScale (1.2f * factor.x, 1.2f * factor.y);
	
	#ifdef BYS_DESKTOP
	
	playBtn.onMouseEntered.connect (bind (&Button::scale, &playBtn, onMouseScale));
	playBtn.onMouseEntered.connect (bind (&Sound::play, &bubbleSound));
	playBtn.onMouseLeave.connect (bind (&Button::scale, &playBtn, normalScale));
	
	aboutBtn.onMouseEntered.connect (bind (&Button::scale, &aboutBtn, onMouseScale));
	aboutBtn.onMouseEntered.connect (bind (&Sound::play, &bubbleSound));
	aboutBtn.onMouseLeave.connect (bind (&Button::scale, &aboutBtn, normalScale));
	
	profileBtn.onMouseEntered.connect (bind (&Button::scale, &profileBtn, onMouseScale));
	profileBtn.onMouseEntered.connect (bind (&Sound::play, &bubbleSound));
	profileBtn.onMouseLeave.connect (bind (&Button::scale, &profileBtn, normalScale));
	
	musicBtn.onMouseEntered.connect (bind (&Button::scale, &musicBtn, onMouseScale));
	musicBtn.onMouseEntered.connect (bind (&Sound::play, &bubbleSound));
	musicBtn.onMouseLeave.connect (bind (&Button::scale, &musicBtn, normalScale));
	musicBtn.stateChanged.connect (bind (&MainMenu::turnMusic, this, placeholders::_1));
	
	soundsBtn.onMouseEntered.connect (bind (&Button::scale, &soundsBtn, onMouseScale));
	soundsBtn.onMouseEntered.connect (bind (&Sound::play, &bubbleSound));
	soundsBtn.onMouseLeave.connect (bind (&Button::scale, &soundsBtn, normalScale));
	soundsBtn.stateChanged.connect (bind (&MainMenu::turnSounds, this, placeholders::_1));
	
	quitBtn.onMouseEntered.connect (bind (&Button::scale, &quitBtn, onMouseScale));
	quitBtn.onMouseEntered.connect (bind (&Sound::play, &bubbleSound));	
	quitBtn.onMouseLeave.connect (bind (&Button::scale, &quitBtn, normalScale));

	#elif defined BYS_MOBILE

	playBtn.onMousePressed.connect (bind (&Button::scale, &playBtn, onMouseScale));
	playBtn.onMousePressed.connect (bind (&Sound::play, &bubbleSound));
	playBtn.onMouseReleased.connect (bind (&Button::scale, &playBtn, normalScale));

	aboutBtn.onMousePressed.connect (bind (&Button::scale, &aboutBtn, onMouseScale));
	aboutBtn.onMousePressed.connect (bind (&Sound::play, &bubbleSound));
	aboutBtn.onMouseReleased.connect (bind (&Button::scale, &aboutBtn, normalScale));

	profileBtn.onMousePressed.connect (bind (&Button::scale, &profileBtn, onMouseScale));
	profileBtn.onMousePressed.connect (bind (&Sound::play, &bubbleSound));
	profileBtn.onMouseReleased.connect (bind (&Button::scale, &profileBtn, normalScale));

	musicBtn.onMousePressed.connect (bind (&Button::scale, &musicBtn, onMouseScale));
	musicBtn.onMousePressed.connect (bind (&Sound::play, &bubbleSound));
	musicBtn.onMouseReleased.connect (bind (&Button::scale, &musicBtn, normalScale));
	musicBtn.stateChanged.connect (bind (&MainMenu::turnMusic, this, placeholders::_1));

	soundsBtn.onMousePressed.connect (bind (&Button::scale, &soundsBtn, onMouseScale));
	soundsBtn.onMousePressed.connect (bind (&Sound::play, &bubbleSound));
	soundsBtn.onMouseReleased.connect (bind (&Button::scale, &soundsBtn, normalScale));
	soundsBtn.stateChanged.connect (bind (&MainMenu::turnSounds, this, placeholders::_1));

	quitBtn.onMousePressed.connect (bind (&Button::scale, &quitBtn, onMouseScale));
	quitBtn.onMousePressed.connect (bind (&Sound::play, &bubbleSound));
	quitBtn.onMouseReleased.connect (bind (&Button::scale, &quitBtn, normalScale));

	#endif
	
	aboutBtn.onClick.connect (bind (&MainMenu::createScene, this, marathon::AboutScene));
	quitBtn.onClick.connect (bind (&Scene::close, this));
	quitBtn.onClick.connect (bind (&GameTask::setType, this->task, GameTask::TurnOff));
	
	playBtn.onClick.connect (bind (&MainMenu::createScene, this, marathon::StableScene));
}

///////////////////////////////////////////
MainMenu::~MainMenu ()
{
	delete spriteBackground;
	ResourceManager::deleteMusic (ASSETS"sounds/bensound-littleidea.ogg");
	ResourceManager::deleteTexture (ASSETS"backgrounds/blackboard.png");
	ResourceManager::deleteTexture (ASSETS"buttons/buttons_mainMenu.png");
	ResourceManager::deleteFont (ASSETS"fonts/Right Chalk.ttf");
}

///////////////////////////////////////////
GameTask * MainMenu::run ()
{
	while (this->running)
	{
		while (game->window.pollEvent (this->event))
		{
		    switch (event.type)
		    {
	    		#ifdef BYS_DESKTOP
    	
    			case Event::MouseMoved:
    				(playBtn.*playBtn.mouseEvent) (event);
    				(aboutBtn.*aboutBtn.mouseEvent) (event);
    				(profileBtn.*profileBtn.mouseEvent) (event);
    				(musicBtn.*musicBtn.mouseEvent) (event);
    				(soundsBtn.*soundsBtn.mouseEvent) (event);
    		        (quitBtn.*quitBtn.mouseEvent) (event);
    		    break;

    		    case Event::MouseButtonPressed:
    				(playBtn.*playBtn.clickEvent) (event);
    				(aboutBtn.*aboutBtn.clickEvent) (event);
    				(profileBtn.*profileBtn.clickEvent) (event);
    				(musicBtn.*musicBtn.clickEvent) (event);
    				(soundsBtn.*soundsBtn.clickEvent) (event);
    		        (quitBtn.*quitBtn.clickEvent) (event);
				break;

				case Event::MouseButtonReleased:
    				(playBtn.*playBtn.clickEvent) (event);
    				(aboutBtn.*aboutBtn.clickEvent) (event);
    				(profileBtn.*profileBtn.clickEvent) (event);
    				(musicBtn.*musicBtn.clickEvent) (event);
    				(soundsBtn.*soundsBtn.clickEvent) (event);
    		        (quitBtn.*quitBtn.clickEvent) (event);
            
				#elif defined BYS_MOBILE

				case Event::TouchBegan:
	    			(playBtn.*playBtn.clickEvent) (event);
	    			(aboutBtn.*aboutBtn.clickEvent) (event);
	    			(profileBtn.*profileBtn.clickEvent) (event);
	    			(musicBtn.*musicBtn.clickEvent) (event);
	    			(soundsBtn.*soundsBtn.clickEvent) (event);
	    	        (quitBtn.*quitBtn.clickEvent) (event);
				break;

				case Event::TouchEnded:
	    			(playBtn.*playBtn.clickEvent) (event);
	    			(aboutBtn.*aboutBtn.clickEvent) (event);
	    			(profileBtn.*profileBtn.clickEvent) (event);
	    			(musicBtn.*musicBtn.clickEvent) (event);
	    			(soundsBtn.*soundsBtn.clickEvent) (event);
	    	        (quitBtn.*quitBtn.clickEvent) (event);
            
	    	    #endif
		    }
		}
		
	    game->window.clear ();

	    game->window.draw (*spriteBackground);
		game->window.draw (gameTitle);

	    playBtn.draw ();
	    musicBtn.draw ();
	    soundsBtn.draw ();
	    profileBtn.draw ();
	    aboutBtn.draw ();
	    quitBtn.draw ();

	    game->window.display ();
	}
	
	running = true;
	return task;
	
}

///////////////////////////////////////////
#include "AboutMenu.hpp"
#include "StableMenu.hpp"
void MainMenu::createScene (MarathonScenes scene)
{
	switch (scene)
	{
		case marathon::AboutScene:
			this->close ();
			task->setType (GameTask::InsertScene);
			task->setScene (new AboutMenu (game, spriteBackground));
		break;
		
		case marathon::StableScene:
			this->close ();
			task->setType (GameTask::InsertScene);
			task->setScene (new StableMenu (game, spriteBackground));
		break;
	}
}

///////////////////////////////////////////
extern bool music;
void MainMenu::turnMusic (bool musicBtnStatus)
{
	auto & backgroundMusic = ResourceManager::getMusic (ASSETS "sounds/bensound-littleidea.ogg");
	
	if (musicBtnStatus)
	{
		music = false;
		backgroundMusic.pause ();
		musicBtn.setTextureRect (IntRect (429, 100, 100, 100));
	}	
	else
	{	
		music = true;
		backgroundMusic.play ();
		backgroundMusic.setLoop (music);
		musicBtn.setTextureRect (IntRect (229, 0, 100, 100));
	}
}

///////////////////////////////////////////
extern bool soundEffects;
void MainMenu::turnSounds (bool soundsBtnStatus)
{
	auto & bubbleSong = ResourceManager::getMusic (ASSETS"sounds/bubble.ogg");
	
	if (soundsBtnStatus)
	{
		soundEffects = false;
		soundsBtn.setTextureRect (IntRect (429, 200, 100, 100));
	}
	else
	{
		soundEffects = true;
		soundsBtn.setTextureRect (IntRect (429, 0, 100, 100));
	}	
}
