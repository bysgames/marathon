#ifndef MARATHON_SUBJECT_MENU_HPP
#define MARATHON_SUBJECT_MENU_HPP

#include <BYS/Core/Game.hpp>
#include <BYS/Core/ResourceManager.hpp>
#include <BYS/GUI/Button.hpp>
using namespace bys;
using namespace sf;

#include <vector>
#include <string>
using namespace std;

#include "Subject.hpp"
#include "Marathon.hpp"
using namespace marathon;

class SubjectsMenu : public Scene
{
public:
	SubjectsMenu (Game *game, Sprite *spriteBackground, HorseType playerHorseType);
	~SubjectsMenu ();

	virtual GameTask * run ();

private:
	Sprite *spriteBackground, subjects, m [5][5];
	Text sceneTitle;
	Button backBtn, acceptBtn, subjectsBtns [5];
	HorseType playerHorseType;
	vector <Subject> selectedSubjects;

	void selectSubject (Button *button, const string &subjectID, bool checked);
	void changeScene (MarathonScenes scene);
};

#endif // MARATHON_SUBJECT_MENU_HPP
