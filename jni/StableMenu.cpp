#include "StableMenu.hpp"
extern bool music, soundEffects;
extern Vector2f factor;
extern Sound bubbleSound;

////////////////////////////////////////
#include <SFML/Audio/Sound.hpp>
StableMenu::StableMenu (Game *game, Sprite *background) : Scene (game), spriteBackground (background), playerHorseType (Mustang)
{
	auto & buttonsTextures = ResourceManager::getTexture (ASSETS"buttons/globalButtons.png");
	auto & assetsStable = ResourceManager::getTexture (ASSETS"assetsStable.png");
	auto & font = ResourceManager::getFont (ASSETS"fonts/Right Chalk.ttf");
	
	sceneTitle.setString ("ESTABLO");
	sceneTitle.setFont (font);
	sceneTitle.setCharacterSize (100);
	sceneTitle.setScale (factor);
	sceneTitle.setFillColor (Color (162, 201, 244, 255));
	setOriginCenter (sceneTitle);
	sceneTitle.setPosition (683.f * factor.x, 100.f * factor.y);
	
	backBtn.create (new Sprite ( buttonsTextures, IntRect (100, 0, 100, 100)), Vector2f ( 100.f * factor.x, 100.f * factor.y), &game->window);
	backBtn.scale (factor); 
	
	acceptBtn.create (new Sprite ( buttonsTextures, IntRect (0, 0, 100, 100)), Vector2f ( 1266.f * factor.x, 100.f * factor.y), &game->window);
	acceptBtn.scale (factor);
	
	ponyBtn.create (new Sprite ( assetsStable, IntRect (1133, 354, 81, 96)), Vector2f ( 200.f * factor.x, 250.f * factor.y), &game->window, true);
	ponyBtn.scale (factor);
	
	mustangBtn.create (new Sprite ( assetsStable, IntRect (1214, 354, 81, 96)), Vector2f ( 200.f * factor.x, 400.f * factor.y), &game->window, 							true, true);
	mustangBtn.scale (factor);
	
	purasangreBtn.create (new Sprite ( assetsStable, IntRect (1052, 353, 81, 96)), Vector2f ( 200.f * factor.x, 550.f * factor.y), &game->window, 							  true);
	purasangreBtn.scale (factor);
	
	horseSelected.setTexture (assetsStable);
	horseSelected.setTextureRect (IntRect (921, 353, 130, 110));
	horseSelected.setScale (factor);
	setOriginCenter (horseSelected);
	horseSelected.setPosition (185.f * factor.x, 400.f * factor.y);
	
	horseSkills.setTexture (assetsStable);
	horseSkills.setTextureRect (IntRect (921, 0, 815, 353));
	horseSkills.setScale (factor);
	setOriginCenter (horseSkills);
	horseSkills.setPosition (780.f * factor.x, 384.f * factor.y);
	  
	horsesBtnGroup.addButton (&ponyBtn);
	horsesBtnGroup.addButton (&mustangBtn);
	horsesBtnGroup.addButton (&purasangreBtn);
	
	using namespace std;
	Vector2f normalScale (1.f * factor.x, 1.f * factor.y), onMouseScale (1.2f * factor.x, 1.2f * factor.y);
	#ifdef BYS_DESKTOP	

	backBtn.onMouseEntered.connect (bind (&Button::scale, &backBtn, onMouseScale));
	backBtn.onMouseLeave.connect (bind (&Button::scale, &backBtn, normalScale));

	acceptBtn.onMouseEntered.connect (bind (&Button::scale, &acceptBtn, onMouseScale));
	acceptBtn.onMouseLeave.connect (bind (&Button::scale, &acceptBtn, normalScale));

	if (soundEffects)
	{
		backBtn.onMouseEntered.connect (bind (&Sound::play, &bubbleSound));
		acceptBtn.onMouseEntered.connect (bind (&Sound::play, &bubbleSound));
	}
	
	#elif defined BYS_MOBILE

	backBtn.onMousePressed.connect (bind (&Button::scale, &backBtn, onMouseScale));
	backBtn.onMouseReleased.connect (bind (&Button::scale, &backBtn, normalScale));

	acceptBtn.onMousePressed.connect (bind (&Button::scale, &acceptBtn, onMouseScale));
	acceptBtn.onMouseReleased.connect (bind (&Button::scale, &acceptBtn, normalScale));

	if (soundEffects)
	{
		backBtn.onMousePressed.connect (bind (&Sound::play, &bubbleSound));
		acceptBtn.onMousePressed.connect (bind (&Sound::play, &bubbleSound));
		ponyBtn.onMousePressed.connect (bind (&Sound::play, &bubbleSound));
		mustangBtn.onMousePressed.connect (bind (&Sound::play, &bubbleSound));
		purasangreBtn.onMousePressed.connect (bind (&Sound::play, &bubbleSound));
	}

	#endif //BYS_DESKTOP

	backBtn.onClick.connect (bind (&Scene::close, this));
	backBtn.onClick.connect (bind (&GameTask::setType, task, GameTask::RemoveScene));

	acceptBtn.onClick.connect (bind (&StableMenu::createScene, this, SubjectsScene));

	horsesBtnGroup.checkedButtonChanged.connect (bind (&StableMenu::changeHorseSelected, this, placeholders::_1));
}

////////////////////////////////////////
StableMenu::~StableMenu () {}

////////////////////////////////////////
GameTask * StableMenu::run ()
{
	while (running)
	{
		while (game->window.pollEvent (this->event))
		{
			switch (event.type)
			{
				#ifdef BYS_DESKTOP
				
				case Event::MouseMoved:
					(backBtn.*backBtn.mouseEvent) (event);
					(acceptBtn.*acceptBtn.mouseEvent) (event);
					(ponyBtn.*ponyBtn.mouseEvent) (event);
					(mustangBtn.*mustangBtn.mouseEvent) (event);
					(purasangreBtn.*purasangreBtn.mouseEvent) (event);
				break;
				
				case Event::MouseButtonPressed:
					(backBtn.*backBtn.clickEvent) (event);
					(acceptBtn.*acceptBtn.clickEvent) (event);
					(ponyBtn.*ponyBtn.clickEvent) (event);
					(mustangBtn.*mustangBtn.clickEvent) (event);
					(purasangreBtn.*purasangreBtn.clickEvent) (event);
				break;
				
				case Event::MouseButtonReleased:
					(backBtn.*backBtn.clickEvent) (event);
					(acceptBtn.*acceptBtn.clickEvent) (event);
					(ponyBtn.*ponyBtn.clickEvent) (event);
					(mustangBtn.*mustangBtn.clickEvent) (event);
					(purasangreBtn.*purasangreBtn.clickEvent) (event);
					
				#elif defined BYS_MOBILE
				
				case Event::TouchBegan:
					(backBtn.*backBtn.clickEvent) (event);
					(acceptBtn.*acceptBtn.clickEvent) (event);
					(ponyBtn.*ponyBtn.clickEvent) (event);
					(mustangBtn.*mustangBtn.clickEvent) (event);
					(purasangreBtn.*purasangreBtn.clickEvent) (event);
				break;
				
				case Event::TouchEnded:
					(backBtn.*backBtn.clickEvent) (event);
					(acceptBtn.*acceptBtn.clickEvent) (event);
					(ponyBtn.*ponyBtn.clickEvent) (event);
					(mustangBtn.*mustangBtn.clickEvent) (event);
					(purasangreBtn.*purasangreBtn.clickEvent) (event);
					
				#endif //BYS_DESKTOP
				
			}			
		}
		
		game->window.clear ();
		game->window.draw (*spriteBackground);
		game->window.draw (sceneTitle);
		backBtn.draw ();
		acceptBtn.draw ();
		ponyBtn.draw ();
		mustangBtn.draw ();
		purasangreBtn.draw ();
		game->window.draw (horseSkills);
		game->window.draw (horseSelected);
		game->window.display ();
		
	}
	
	running = true;
	return task;	
}

////////////////////////////////////////
void StableMenu::changeHorseSelected (AbstractButton *abstractButton)
{	
	//horseSelected.setPosition (abstractButton->getPosition ());

	if (abstractButton == &ponyBtn)
	{
		horseSkills.setTextureRect (IntRect (10, 0, 910, 345));
		horseSelected.setPosition (185.f * factor.x, 250.f * factor.y);
		playerHorseType = Pony;
		return;
	}

	if (abstractButton == &purasangreBtn)
	{
		horseSkills.setTextureRect (IntRect (0, 345, 921, 380));
		horseSelected.setPosition (185.f * factor.x, 550.f * factor.y);
		playerHorseType = Purasangre;
		return;
	}

	horseSkills.setTextureRect (IntRect (921, 0, 815, 353));
	horseSelected.setPosition (185.f * factor.x, 400.f * factor.y);
	playerHorseType = Mustang;
}

////////////////////////////////////////
#include "SubjectsMenu.hpp"
void StableMenu::createScene (MarathonScenes scene)
{
	this->close ();
	task->setType (GameTask::InsertScene);
	task->setScene (new SubjectsMenu (game, spriteBackground, playerHorseType));
}
