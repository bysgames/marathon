#ifndef MARATHON_HIPODROME_HPP
#define MARATHON_HIPODROME_HPP

#include <SFML/Graphics/Text.hpp>
#include <SFML/Audio/Music.hpp>
using namespace sf;

#include <BYS/Core/Scene.hpp>
#include <BYS/Core/ResourceManager.hpp>
#include <BYS/GUI/HUD.hpp>
using namespace bys;

#include "Horse.hpp"
#include "Subject.hpp"
#include "Marathon.hpp"
using namespace marathon;

#include <vector>
using namespace std;

class Hipodrome : public Scene
{
public:
	Hipodrome (Game *game, HorseType playerHorseType, vector <Subject> &subjects);
	~Hipodrome ();

	virtual GameTask * run ();

private:
	Sprite spriteBackground;
	Horse playerHorse, cpuHorse;
//	Text cpuCoordinates, playerCoordinates;
	HorseType horseUnderHUD, playerHorseType;
	HUD hud;
	vector <Subject> subjects;

	void createScene (MarathonScenes scene);
	void repositionHUD (HorseType horseRequestingMove);
	void setPositionText (Text *text, Horse *horse);
};

#endif // MARATHON_HIPODROME_HPP
