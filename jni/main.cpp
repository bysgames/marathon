#include <BYS/Core/Game.hpp>
#include <BYS/Core/ResourceManager.hpp>
using namespace bys;

#include <SFML/Audio/Sound.hpp>
using namespace sf;

#include "MainMenu.hpp"

ResourceManager resourceManager;
bool music = true, soundEffects = true;
Vector2f factor;
Sound bubbleSound;

int main ()
{
	auto mode = sf::VideoMode::getDesktopMode ();
	factor.x = mode.width / 1366.f;
	factor.y = mode.height / 768.f;

	bubbleSound.setBuffer (ResourceManager::getSoundBuffer (ASSETS"sounds/bubble.ogg"));
	
	srand (time (NULL));

	Game marathon (mode, "Marathon");
    return marathon.run (new MainMenu (&marathon));
}
