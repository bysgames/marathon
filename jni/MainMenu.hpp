#ifndef MARATHON_MAIN_MENU_HPP
#define MARATHON_MAIN_MENU_HPP

#include <BYS/Core/Game.hpp>
#include <BYS/Core/GameTask.hpp>
#include <BYS/Core/Scene.hpp>
#include <BYS/Core/ResourceManager.hpp>
#include <BYS/GUI/Button.hpp>
#include <SFML/Audio/Music.hpp>
using namespace bys;
using namespace sf;

#include "Marathon.hpp"
using namespace marathon;

class MainMenu : public Scene
{
public:
    MainMenu (Game *game);
    ~MainMenu ();

	virtual GameTask * run ();
	
	void turnMusic (bool musicBtnStatus);
	void turnSounds (bool soundsBtnStatus);

    void createScene (marathon::MarathonScenes scene);

private:
    Sprite *spriteBackground;
    Text gameTitle;
    Button quitBtn, playBtn, musicBtn, soundsBtn, profileBtn, aboutBtn;
};

#endif // MARATHON_MAIN_MENU_HPP
