#ifndef MARATHON_ABOUT_MENU_HPP
#define MARATHON_ABOUT_MENU_HPP

#include <BYS/Core/Scene.hpp>
#include <BYS/Core/Game.hpp>
#include <BYS/Core/ResourceManager.hpp>
#include <BYS/GUI/Button.hpp>
using namespace bys;
using namespace sf;

#include "Marathon.hpp"
using namespace marathon;

class AboutMenu : public Scene
{
public:
	AboutMenu (Game *game, Sprite *spriteBackground);
	~AboutMenu ();
	
	virtual GameTask * run ();
	
	void createScene (MarathonScenes scene);
	
private:
	Button backBtn, rulesBtn, creditsBtn;
	Sprite *spriteBackground;
};

#endif // MARATHON_ABOUT_MENU_HPP
