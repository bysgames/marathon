#ifndef MARATHON_HORSE_HPP
#define MARATHON_HORSE_HPP

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/View.hpp>
using namespace sf;

#include <BYS/Core/Signal.hpp>
#include <BYS/GUI/HUD.hpp>
using namespace bys;

#include "Marathon.hpp"
using namespace marathon;

class Horse : public Sprite
{
public:
	Horse ();
	Horse (const Texture &texture, const IntRect &rect, HorseType type);
	void create (const Texture &texture, const IntRect &rect, HorseType type);

	void movePositions (int steps);
	const Vector2f & getCurrentPosition () const;
	int getIteratorPosition () const;
	Vector2f positions [36];
	void update (HUD *hud);
	inline HorseType getType () const {  return type;  }
	char getDifficulty ();

	inline bool isMoving () const {  return (steps > 0) ? true : false;  }

	Signal <> hasWon;
	Signal <> moveRequest;
	Signal <> stopRequest;

private:
	enum MovementType {Up, Down, Left, Right, Zero} movementX, movementY;

	int currentPosition, steps;
	HorseType type;
	Vector2f delta, speed;
	bool toLeft;

	void updateDeltaAndSpeed ();
};

#endif // MARATHON_HORSE_HPP
