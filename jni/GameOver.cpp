#include "GameOver.hpp"

#include <SFML/Audio/Sound.hpp>
extern Sound bubbleSound;
extern bool soundEffects;
extern Vector2f factor;

////////////////////////////////////////////////////
#include <BYS/Core/ResourceManager.hpp>
#include <string>
#include "sqlite/sqlite3.h"
GameOver::GameOver (Game *game, vector <Subject> &subjects, Horse *winner) : Scene (game)
{
	//////////////////////////////////////////////// Assets
	auto & font = ResourceManager::getFont (ASSETS"fonts/cafe&brewery.ttf");

	//////////////////////////////////////////////// Background and scene title
	spriteBackground.setTexture (ResourceManager::getTexture (ASSETS"backgrounds/paper.png"));
	spriteBackground.setScale (factor);

	if (winner->getType () != CPU)
		sceneTitle.setString ("Ganaste :D");
	else
		sceneTitle.setString ("Perdiste :(");
	
	sceneTitle.setFont (ResourceManager::getFont (ASSETS"fonts/Right Chalk.ttf"));

	//////////////////////////////////////////////// Accept button
	acceptButton.create (new Sprite (ResourceManager::getTexture (ASSETS"buttons/gameplay.png"), IntRect (0, 156, 293, 91)),
						 new Text ("Aceptar", font, 60U), Vector2f (683.f * factor.x, 450.f * factor.y), &game->window);
	acceptButton.scale (factor);

	#ifdef BYS_DESKTOP
	acceptButton.onMouseEntered.connect (bind (&Button::scale, &acceptButton, Vector2f (1.1f * factor.x, 1.1f * factor.y)));
	acceptButton.onMouseLeave.connect (bind (&Button::scale, &acceptButton, Vector2f (1.f * factor.x, 1.f * factor.y)));

	if (soundEffects)
		acceptButton.onMouseEntered.connect (bind (&Sound::play, &bubbleSound));
	#elif defined BYS_MOBILE
	acceptButton.onMousePressed.connect (bind (&Button::scale, &acceptButton, Vector2f (1.1f * factor.x, 1.1f * factor.y)));
	acceptButton.onMouseReleased.connect (bind (&Button::scale, &acceptButton, Vector2f (1.f * factor.x, 1.f * factor.y)));

	if (soundEffects)
		acceptButton.onMousePresed.connect (bind (&Sound::play, &bubbleSound));
	#endif
	acceptButton.onClick.connect (bind (&GameOver::close, this));

	//////////////////////////////////////////////// Texts
	totalQuestionsText.setString ("Total de preguntas");
	totalQuestionsText.setFont (font);
	totalQuestionsText.setCharacterSize (40U);
	setOriginCenter (totalQuestionsText);
	totalQuestionsText.setScale (factor);
	totalQuestionsText.setPosition (400.f * factor.x, 250.f * factor.y);

	totalCorrectAnswersText.setString ("Respuestas correctas");
	totalCorrectAnswersText.setFont (font);
	totalCorrectAnswersText.setCharacterSize (40U);
	setOriginCenter (totalCorrectAnswersText);
	totalCorrectAnswersText.setScale (factor);
	totalCorrectAnswersText.setPosition (650.f * factor.x, 250.f * factor.y);

	qualificationText.setString ("Calificación");
	qualificationText.setFont (font);
	qualificationText.setCharacterSize (40U);
	setOriginCenter (qualificationText);
	qualificationText.setScale (factor);
	qualificationText.setPosition (900.f * factor.x, 250.f * factor.y);

	//////////////////////////////////////////////// Player's results
	totalSubjects = subjects.size ();

	playerResults = new Text *[totalSubjects];
	for (int i = 0; i < totalSubjects; ++i)
		for (int j = 0; j < 4; ++j)
			playerResults [i] = new Text;

	//////////////////////// Subjects' id
	for (int i = 0; i < totalSubjects; ++i)
	{
		playerResults [i] [0].setString (subjects [i].id);
		playerResults [i] [0].setFont (font);
		playerResults [i] [0].setCharacterSize (40U);
		setOriginCenter (playerResults [i] [0]);
		playerResults [i] [0].setScale (factor);
		playerResults [i] [0].setPosition (150.f * factor.x, (300.f + i * 100.f) * factor.y);
	}

	//////////////////////// Subject's total of questions
	for (int i = 0; i < totalSubjects; ++i)
	{
		playerResults [i] [1].setString (to_string (subjects [i].totalOfQuestions));
		playerResults [i] [1].setFont (font);
		playerResults [i] [1].setCharacterSize (40U);
		setOriginCenter (playerResults [i] [1]);
		playerResults [i] [1].setScale (factor);
		playerResults [i] [1].setPosition (400.f * factor.x, (300.f + i * 100.f) * factor.y);
	}

	//////////////////////// Subject's correct answers
	for (int i = 0; i < totalSubjects; ++i)
	{
		playerResults [i] [2].setString (to_string (subjects [i].correctAnswers));
		playerResults [i] [2].setFont (font);
		playerResults [i] [2].setCharacterSize (40U);
		setOriginCenter (playerResults [i] [1]);
		playerResults [i] [2].setScale (factor);
		playerResults [i] [2].setPosition (650.f * factor.x, (300.f + i * 100.f) * factor.y);
	}

	//////////////////////// Subject's qualification
	// If the player reached more than 60 % then he unlocks the subjects on the next grade.
	sqlite3 *database;
	sqlite3_stmt *statement;

	#ifdef BYS_DESKTOP
	sqlite3_open (ASSETS"files/marathon", &database);
	#elif defined BYS_MOBILE
	sqlite3_open ("/data/data/com.bysgames.marathon/files/marathon.db", &database);
	#endif
	
	for (int i = 0; i < totalSubjects; ++i)
	{
		int qualification = subjects [i].correctAnswers * 100 / subjects [i].totalOfQuestions;
		
		if (qualification > 60)
		{
			playerResults [i] [1].setFillColor (Color::Green);

			string query ("UPDATE Subject SET State = 'Approved' WHERE ID = '");
			query += subjects [i].id;
			query += "';";
			sqlite3_prepare (database, query.c_str (), 0, &statement, 0);

			query = "UPDATE Subject SET State = 'Enable' WHERE ID = '";
			string newID (subjects [i].id);
			newID.replace (2, 1, to_string (atoi ((const char *)newID [3]) + 1));
			query += newID;
			query += "';";

			sqlite3_prepare (database, query.c_str (), 0, &statement, 0);
		}
		else
			playerResults [i] [1].setFillColor (Color::Red);

		playerResults [i] [1].setString (to_string (qualification));
		playerResults [i] [1].setFont (font);
		playerResults [i] [1].setCharacterSize (40U);
		setOriginCenter (playerResults [i] [1]);
		playerResults [i] [1].setScale (factor);
		playerResults [i] [1].setPosition (150.f * factor.x, (300.f + i * 100.f) * factor.y);
	}

	sqlite3_close (database);
}

////////////////////////////////////////////////////
GameOver::~GameOver ()
{
	for (int i = 0; i < totalSubjects; ++i)
		delete playerResults [i];
	delete playerResults;
}

////////////////////////////////////////////////////
GameTask * GameOver::run ()
{
	while (this->running)
	{
		while (game->window.pollEvent (event))
			switch (event.type)
			{
				#ifdef BYS_DESKTOP
				case Event::MouseMoved:
					(acceptButton.*acceptButton.mouseEvent) (event);
				break;

				case Event::MouseButtonPressed:
					(acceptButton.*acceptButton.clickEvent) (event);
				break;

				case Event::MouseButtonReleased:
					(acceptButton.*acceptButton.clickEvent) (event);
				#elif defined BYS_MOBILE
				case Event::TouchBegan:
					(acceptButton.*acceptButton.clickEvent) (event);
				break;

				case Event::TouchEnded:
					(acceptButton.*acceptButton.clickEvent) (event);
				#endif
			}

		game->window.clear ();
		game->window.draw (spriteBackground);
		game->window.draw (sceneTitle);
		game->window.draw (qualificationText);
		game->window.draw (totalQuestionsText);
		game->window.draw (totalCorrectAnswersText);

		for (int i = 0; i < totalSubjects; ++i)
			for (int j = 0; j < 4; ++j)
				game->window.draw (playerResults [i] [j]);

		acceptButton.draw ();
		game->window.display ();
	}

	return task;
}

////////////////////////////////////////////////////
#include "MainMenu.hpp"
void GameOver::close ()
{
	Scene::close ();
	task->setType (GameTask::ReplaceScenes);
	task->setScene (new MainMenu (game));
}
