#ifndef MARATHON_GAME_OVER_HPP
#define MARATHON_GAME_OVER_HPP

#include <BYS/Core/Scene.hpp>
#include <BYS/Core/GameTask.hpp>
#include <BYS/GUI/Button.hpp>
using namespace bys;

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Sprite.hpp>
using namespace sf;

#include <vector>
using namespace std;

#include "Subject.hpp"
#include "Marathon.hpp"
#include "Horse.hpp"

class GameOver : public Scene
{
public:
	GameOver (Game *game, vector <Subject> &subjects, Horse *winner);
	~GameOver ();
	virtual GameTask * run ();

private:
	Sprite spriteBackground;
	Text sceneTitle, totalQuestionsText, totalCorrectAnswersText, qualificationText, **playerResults;
	int totalSubjects;
	Button acceptButton;

	void close ();
};

#endif // MARATHON_GAME_OVER_HPP
