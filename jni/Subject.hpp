#ifndef MARATHON_SUBJECT_HPP
#define MARATHON_SUBJECT_HPP

#include <string>
using namespace std;

struct Subject
{
	string id;
	int totalOfQuestions;
	int correctAnswers;

	Subject (const string &id) : id (id), totalOfQuestions (0), correctAnswers (0) {}
};

#endif // MARATHON_SUBJECT_HPP
