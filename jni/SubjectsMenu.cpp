#include "SubjectsMenu.hpp"
extern bool music, soundEffects;
extern Vector2f factor;
extern Sound bubbleSound;

using namespace std;

///////////////////////////////////////////
#include "sqlite/sqlite3.h"
#include <SFML/Audio/Sound.hpp>
SubjectsMenu::SubjectsMenu (Game *game, Sprite *spriteBackground, HorseType playerHorseType) 
							: Scene (game), spriteBackground (spriteBackground), playerHorseType (playerHorseType)
{
	auto & textures = ResourceManager::getTexture (ASSETS"subjectMenuTextures.png");
	auto & buttonsTextures = ResourceManager::getTexture (ASSETS"buttons/globalButtons.png");
	auto & font = ResourceManager::getFont (ASSETS"fonts/Right Chalk.ttf");

	sceneTitle.setFont (font);
	sceneTitle.setString ("MATERIAS");
	sceneTitle.setCharacterSize (100U);
	sceneTitle.setFillColor (Color (162, 201, 244, 255));
	sceneTitle.setScale (factor);
	setOriginCenter (sceneTitle);
	sceneTitle.setPosition (683.f * factor.x, 100.f * factor.y);

	subjects.setTexture (textures);
	subjects.setTextureRect (IntRect (0, 0, 1118, 414));
	subjects.setScale (factor);
	setOriginCenter (subjects);
	subjects.setPosition (683.f * factor.x, 434.f * factor.y);

	backBtn.create (new Sprite (buttonsTextures, IntRect (100, 0, 100, 100)), Vector2f (100.f * factor.x, 100.f * factor.y), &game->window);
	backBtn.scale (factor);
	backBtn.onClick.connect (bind (&SubjectsMenu::changeScene, this, StableScene));

	acceptBtn.create (new Sprite (buttonsTextures, IntRect (0, 0, 100, 100)), Vector2f (1266.f * factor.x, 100.f * factor.y), &game->window);
	acceptBtn.scale (factor);
	acceptBtn.onClick.connect (bind (&SubjectsMenu::changeScene, this, HipodromeScene));

	Vector2f normalScale (1.f * factor.x, 1.f * factor.y), onMouseScale (1.2f * factor.x, 1.2f * factor.y);
	auto & bubbleSong = ResourceManager::getMusic (ASSETS"sounds/bubble.ogg");
	#ifdef BYS_DESKTOP	

	backBtn.onMouseEntered.connect (bind (&Button::scale, &backBtn, onMouseScale));
	backBtn.onMouseLeave.connect (bind (&Button::scale, &backBtn, normalScale));

	acceptBtn.onMouseEntered.connect (bind (&Button::scale, &acceptBtn, onMouseScale));
	acceptBtn.onMouseLeave.connect (bind (&Button::scale, &acceptBtn, normalScale));

	if (soundEffects)
	{
		backBtn.onMouseEntered.connect (bind (&Sound::play, &bubbleSound));
		acceptBtn.onMouseEntered.connect (bind (&Sound::play, &bubbleSound));
	}

	#elif defined BYS_MOBILE

	backBtn.onMousePressed.connect (bind (&Button::scale, &backBtn, onMouseScale));
	backBtn.onMouseReleased.connect (bind (&Button::scale, &backBtn, normalScale));

	acceptBtn.onMousePressed.connect (bind (&Button::scale, &acceptBtn, onMouseScale));
	acceptBtn.onMouseReleased.connect (bind (&Button::scale, &acceptBtn, normalScale));

	if (soundEffects)
	{
		backBtn.onMousePressed.connect (bind (&Sound::play, &bubbleSound));
		acceptBtn.onMousePressed.connect (bind (&Sound::play, &bubbleSound));
	}

	#endif // BYS_DESKTOP

	sqlite3 *database;
	sqlite3_stmt *statement;

	#ifdef BYS_DESKTOP
	if (sqlite3_open (ASSETS"files/marathon", &database) == SQLITE_OK)
	#elif defined BYS_MOBILE
	if (sqlite3_open ("/data/data/com.bysgames.marathon/files/marathon.db", &database) == SQLITE_OK)
	#endif
	{
		sqlite3_prepare (database, "SELECT ID, Grade FROM Subject WHERE State = 'Enable';", 100, &statement, 0);

		for (int i = 0; sqlite3_step (statement) == SQLITE_ROW; ++i)
		{
			int enabledGrade = sqlite3_column_int (statement, 1) - 1;

			subjectsBtns [i].create (new Sprite (textures, IntRect (130, 414, 130, 55)),
  					 			     Vector2f ((507.f + 133.f * enabledGrade) * factor.x,
											   (379.f + 58 * i) * factor.y), &game->window, true);
			subjectsBtns [i].scale (factor);
			subjectsBtns [i].stateChanged.connect (bind (&SubjectsMenu::selectSubject, this, &subjectsBtns [i],
												   string (reinterpret_cast <const char *> (sqlite3_column_text (statement, 0))),
												   placeholders::_1));

			for (int j = 0; j < enabledGrade; ++j)
			{
				m [i][j].setTexture (textures);
				m [i][j].setTextureRect (IntRect (0, 414, 130, 55));
				setOriginCenter (m [i][j]);
				m [i][j].setScale (factor);
				m [i][j].setPosition ((507.f + 133.f * j) * factor.x, (379.f + 58 * i) * factor.y);
			}

			for (int j = enabledGrade; j < 5; ++j)
			{
				m [i][j].setTexture (textures);
				m [i][j].setTextureRect (IntRect (390, 414, 130, 55));
				setOriginCenter (m [i][j]);
				m [i][j].setScale (factor);
				m [i][j].setPosition ((507.f + 133.f * (j + 1)) * factor.x, (379.f + 58 * i) * factor.y);
			}
		}

		sqlite3_finalize (statement);
		sqlite3_close (database);
	}
}

///////////////////////////////////////////
SubjectsMenu::~SubjectsMenu () {}

///////////////////////////////////////////
GameTask * SubjectsMenu::run ()
{
	while (this->running)
	{
		while (game->window.pollEvent (this->event))
		{
			switch (event.type)
			{
				#ifdef BYS_DESKTOP
				case Event::Closed: break;

				case Event::MouseMoved:
					(backBtn.*backBtn.mouseEvent) (event);
					(acceptBtn.*acceptBtn.mouseEvent) (event);
					for (int i = 0; i < 5; ++i)
						(subjectsBtns [i].*subjectsBtns [i].mouseEvent) (event);
				break;

				case Event::MouseButtonPressed:
					(backBtn.*backBtn.clickEvent) (event);
					(acceptBtn.*acceptBtn.clickEvent) (event);
					for (int i = 0; i < 5; ++i)
						(subjectsBtns [i].*subjectsBtns [i].clickEvent) (event);
				break;

				case Event::MouseButtonReleased:
					(backBtn.*backBtn.clickEvent) (event);
					(acceptBtn.*acceptBtn.clickEvent) (event);
					for (int i = 0; i < 5; ++i)
						(subjectsBtns [i].*subjectsBtns [i].clickEvent) (event);

				#elif defined BYS_MOBILE

				case Event::TouchBegan:
					(backBtn.*backBtn.clickEvent) (event);
					(acceptBtn.*acceptBtn.clickEvent) (event);
					for (int i = 0; i < 5; ++i)
						(subjectsBtns [i].*subjectsBtns [i].clickEvent) (event);
				break;

				case Event::TouchEnded:
					(backBtn.*backBtn.clickEvent) (event);
					(acceptBtn.*acceptBtn.clickEvent) (event);
					for (int i = 0; i < 5; ++i)
						(subjectsBtns [i].*subjectsBtns [i].clickEvent) (event);
				#endif
			}
		}

		game->window.clear ();

		game->window.draw (*spriteBackground);

		game->window.draw (sceneTitle);
		game->window.draw (subjects);
		backBtn.draw ();
		acceptBtn.draw ();

		for (int i = 0; i < 5; ++i)
		{
			subjectsBtns [i].draw ();
			for (int j = 0; j < 5; ++j)
				game->window.draw (m [i][j]);
		}

		game->window.display ();
	}

	return task;
}

///////////////////////////////////////////
void SubjectsMenu::selectSubject (Button *button, const string &subjectID, bool checked)
{
	if (checked)
	{
		button->setTextureRect (IntRect (260, 414, 130, 55));
		selectedSubjects.push_back (Subject (subjectID));
	}
	else
	{
		button->setTextureRect (IntRect (130, 414, 130, 55));
		for (auto i = selectedSubjects.begin (); i != selectedSubjects.end (); ++i)
		{
			if (i->id == subjectID)
			{
				selectedSubjects.erase (i);
				return;
			}
		}
	}
}

///////////////////////////////////////////
#include "Hipodrome.hpp"
void SubjectsMenu::changeScene (MarathonScenes scene)
{
	switch (scene)
	{
		case StableScene:
			this->close ();
			task->setType (GameTask::RemoveScene);
		break;

		case HipodromeScene:
			this->close ();
			task->setType (GameTask::ReplaceScenes);
			task->setScene (new Hipodrome (game, playerHorseType, selectedSubjects));
	}
}
