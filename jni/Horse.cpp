#include "Horse.hpp"

#include <iostream>

////////////////////////////////////////////////////
Horse::Horse () : currentPosition (-1), steps (0), toLeft (true) {}

////////////////////////////////////////////////////
Horse::Horse (const Texture &texture, const IntRect &rect, HorseType type) : Sprite (texture, rect), currentPosition (-1), type (type) {}

////////////////////////////////////////////////////
void Horse::create (const Texture &texture, const IntRect &rect, HorseType type)
{
	Sprite::setTexture (texture);
	Sprite::setTextureRect (rect);

	currentPosition = 0;
	this->type = type;
}

////////////////////////////////////////////////////
void Horse::movePositions (int steps)
{
	if (currentPosition + steps <= 36)
		this->steps = steps;
	else
		this->steps = 36 - currentPosition;

	updateDeltaAndSpeed ();
	moveRequest.emit ();
}

////////////////////////////////////////////////////
const Vector2f & Horse::getCurrentPosition () const {  return positions [currentPosition];  }

////////////////////////////////////////////////////
int Horse::getIteratorPosition () const {  return currentPosition;  }

////////////////////////////////////////////////////
void Horse::update (HUD *hud)
{
	if (movementX != Zero || movementY != Zero)
	{
		this->move (speed);
		hud->move (speed);

		switch (movementX)
		{
			case Left:
				if (! toLeft)
				{
					this->setScale (1.f, 1.f);
					toLeft = true;
				}

				if ((delta.x += speed.x) <= 0)
				{
					speed.x = 0.f;
					movementX = Zero;
				}
			break;

			case Right:
				if (toLeft)
				{
					this->setScale (-1.f, 1.f);
					toLeft = false;
				}

				if ((delta.x += speed.x) >= 0)
				{
					speed.x = 0.f;
					movementX = Zero;
				}
		}

		switch (movementY)
		{
			case Up:   if ((delta.y += speed.y) <= 0) { speed.y = 0.f;  movementY = Zero; } break;
			case Down: if ((delta.y += speed.y) >= 0) { speed.y = 0.f;  movementY = Zero; }
		}
	}
	else
	{
		++currentPosition;
		--steps;
		updateDeltaAndSpeed ();
	}
}

////////////////////////////////////////////////////
#include <math.h>
void Horse::updateDeltaAndSpeed ()
{
	if (steps > 0)
	{
		delta.x = positions [currentPosition + 1].x - positions [currentPosition].x;
		delta.y = positions [currentPosition + 1].y - positions [currentPosition].y;

		float deltaXAbs = fabs (delta.x);
		float deltaYAbs = fabs (delta.y);

		if (deltaYAbs)
		{
			speed.x = deltaXAbs / deltaYAbs * 5.f;
			speed.y = 5.f;
		}
		else
		{
			speed.x = 5.f;
			speed.y = 0.f;
		}

		if (delta.x < 0)
		{
			speed.x *= -1.f;
			movementX = Left;
		}
		else if (delta.x > 0)
			movementX = Right;
		else
		{
			speed.x = 0.f;
			movementX = Zero;
		}

		if (delta.y < 0)
		{
			speed.y *= -1.f;
			movementY = Up;
		}
		else if (delta.y > 0)
			movementY = Down;
		else if (delta.y == 0)
		{
			speed.y = 0.f;
			movementY = Zero;
		}

		delta.x *= -1;
		delta.y *= -1;
	}
	else
	{
		stopRequest.emit ();

		if (currentPosition == 36)
			hasWon.emit ();
	}
}

////////////////////////////////////////////////////
#include <stdlib.h>
char Horse::getDifficulty ()
{
	int randomDifficulty;
		
		switch (type)
		{
			case Pony :
				randomDifficulty = rand () % 4;
				
				if (randomDifficulty <= 1)
					return 1;
				
				else
					return randomDifficulty + 1;
			break;
			
			case Mustang:
				randomDifficulty = rand () % 3;
				
				return randomDifficulty + 1;
			break;
			
			case Purasangre:
				randomDifficulty = rand () % 4;
				
				if (randomDifficulty >= 2)
					return 3;
				
				else
					return randomDifficulty + 1;
		}
}

