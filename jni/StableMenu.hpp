#ifndef MARATHON_STABLE_MENU_HPP
#define MARATHON_STABLE_MENU_HPP

#include <BYS/Core/Game.hpp>
#include <BYS/Core/GameTask.hpp>
#include <BYS/Core/Scene.hpp>
#include <BYS/Core/ResourceManager.hpp>
#include <BYS/GUI/Button.hpp>
#include <BYS/GUI/ButtonGroup.hpp>
using namespace bys;
using namespace sf;

#include "Marathon.hpp"
using namespace marathon;

class StableMenu: public Scene
{
public:
	StableMenu (Game *game, Sprite *background);
	~StableMenu ();
	
	virtual GameTask *run ();
	
	void changeHorseSelected (AbstractButton *abstractButton);
		
	void createScene (MarathonScenes scene);
	
private:
	Text sceneTitle;
	Sprite *spriteBackground, horseSelected, horseSkills;
	Button backBtn, acceptBtn, ponyBtn, mustangBtn, purasangreBtn;
	ButtonGroup horsesBtnGroup;

	HorseType playerHorseType;
};

#endif //MARATHON_STABLE_MENU_HPP

