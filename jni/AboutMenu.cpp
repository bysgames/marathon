#include "AboutMenu.hpp"
extern bool music, soundEffects;
extern Vector2f factor;

////////////////////////////////////////////////////
AboutMenu::AboutMenu (Game *game, Sprite *spriteBackground) : Scene (game), spriteBackground (spriteBackground)
{
	auto & buttonsTexture = ResourceManager::getTexture (ASSETS"buttons/globalButtons.png");
	
	backBtn.create (new Sprite (buttonsTexture, IntRect (100, 0, 100, 100)), Vector2f (120 * factor.x, 672 * factor.y), &game->window);
	backBtn.scale (factor);
	rulesBtn.create (new Sprite (buttonsTexture, IntRect (0, 117, 612, 146)), Vector2f (683, 300), &game->window);
	creditsBtn.create (new Sprite (buttonsTexture, IntRect (0, 117, 612, 146)), Vector2f (683, 500), &game->window);

	using namespace std;
	Vector2f normalScale (1.f * factor.x, 1.f * factor.y), onMouseScale (1.2f * factor.x, 1.2f * factor.y);
	
	#ifdef BYS_DESKTOP
	backBtn.onMouseEntered.connect (bind (&Button::scale, &backBtn, onMouseScale));
	backBtn.onMouseLeave.connect (bind (&Button::scale, &backBtn, normalScale));

	rulesBtn.onMouseEntered.connect (bind (&Button::scale, &rulesBtn, onMouseScale));
	rulesBtn.onMouseLeave.connect (bind (&Button::scale, &rulesBtn, normalScale));

	creditsBtn.onMouseEntered.connect (bind (&Button::scale, &creditsBtn, onMouseScale));
	creditsBtn.onMouseLeave.connect (bind (&Button::scale, &creditsBtn, normalScale));
	#endif

	backBtn.onClick.connect (bind (&GameTask::setType, this->task, GameTask::RemoveScene));
	backBtn.onClick.connect (bind (&Scene::close, this));
}

AboutMenu::~AboutMenu () {}

////////////////////////////////////////////////////
GameTask * AboutMenu::run ()
{
	while (this->running)
	{
		while (game->window.pollEvent (this->event))
		{
			switch (event.type)
			{
				#ifdef BYS_DESKTOP
				case Event::MouseMoved:
					(backBtn.*backBtn.mouseEvent) (event);
				break;

				case Event::MouseButtonPressed:
					(backBtn.*backBtn.clickEvent) (event);
				break;
				
				case Event::MouseButtonReleased:
					(backBtn.*backBtn.clickEvent) (event);
				
				#elif defined BYS_MOBILE
				case Event::TouchBegan:
					(backBtn.*backBtn.clickEvent) (event);
				break;
				
				case Event::TouchEnded:
					(backBtn.*backBtn.clickEvent) (event);
				break;
				#endif
			}
		}

		game->window.clear ();
		game->window.draw (*spriteBackground);
		backBtn.draw ();
		game->window.display ();
	}
	
	running = true;
	return task;
}

////////////////////////////////////////////////////
void AboutMenu::createScene (MarathonScenes scene)
{
	switch (scene)
	{
		case marathon::InstructionsScene:
		break;
		
		case marathon::CreditsScene:
		break;
	}
}
