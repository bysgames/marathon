#ifndef MARATHON_QUESTION_MENU_HPP
#define MARATHON_QUESTION_MENU_HPP

#include <BYS/Core/Game.hpp>
#include <BYS/Core/ResourceManager.hpp>
#include <BYS/GUI/Button.hpp>
#include <BYS/GUI/ButtonGroup.hpp>
using namespace bys;
using namespace sf;

#include "Horse.hpp"
#include "Subject.hpp"

#include "Marathon.hpp"
using namespace marathon;

class QuestionMenu : public Scene
{
public:

	QuestionMenu (Game *game, Horse *cpuHorse, Horse *playerHorse, std::vector <Subject> &subjects);
	~QuestionMenu ();
	
	virtual GameTask * run ();
	
	void changeTextureQuestionsBtns (AbstractButton *abstractButton);
	void checkAnswer ();

private:

	int difficulty, randomSubject;
	Button *correctAnswer;
	Button answers [4];
	ButtonGroup answersBG;
	Button readyBtn;
	Sprite spriteBackground, underlined;
	Text question;	
	Vector2f positions [4];
	Horse *playerHorse, *cpuHorse;
	vector <Subject> * subjects; 
	
	const View *view;
};

#endif // MARATHON_QUESTION_MENU_HPP
