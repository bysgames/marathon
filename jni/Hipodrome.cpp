#include "Hipodrome.hpp"

extern Vector2f factor;
extern Sound bubbleSound;
extern bool soundEffects, music;

////////////////////////////////////////////////////
#include <string>
#include <fstream>
#include <iostream>
#include <SFML/Audio/Sound.hpp>
#include <BYS/GUI/Button.hpp>
using namespace std;

Hipodrome::Hipodrome (Game *game, HorseType playerHorseType, vector <Subject> &subjects)
					 : Scene (game), playerHorseType (playerHorseType), subjects (subjects)
{
									////////// The background //////////
	spriteBackground.setTexture (ResourceManager::getTexture (ASSETS"backgrounds/hipodrome.png"));
	spriteBackground.setScale (factor);

									////////// The horses //////////
	auto & cpuHorseTexture = ResourceManager::getTexture (ASSETS"horses/ignorance.png");
	cpuHorse.create (cpuHorseTexture, IntRect (78, 0, 185, 177), CPU);
	setOriginCenter (cpuHorse);
	cpuHorse.setScale (factor);
	cpuHorse.hasWon.connect (bind (&Hipodrome::createScene, this, GameOverScene));

	Texture *playerHorseTexture;
	switch (playerHorseType)
	{
		case Pony:
			playerHorseTexture = &ResourceManager::getTexture (ASSETS"horses/pony.png");
			playerHorse.create (*playerHorseTexture, IntRect (78, 0, 188, 170), Pony);
			break;

		case Mustang:
			playerHorseTexture = &ResourceManager::getTexture (ASSETS"horses/mustang.png");
			playerHorse.create (*playerHorseTexture, IntRect (78, 0, 185, 177), Mustang);
			break;

		case Purasangre:
			playerHorseTexture = &ResourceManager::getTexture (ASSETS"horses/purasangre.png");
			playerHorse.create (*playerHorseTexture, IntRect (78, 0, 215, 205), Purasangre);
	}
	setOriginCenter (playerHorse);
	playerHorse.setScale (factor);
	playerHorse.hasWon.connect (bind (&Hipodrome::createScene, this, GameOverScene));

	ifstream filePositionsCpu (ASSETS"files/cpuPositions");
	ifstream filePositionsPlayer (ASSETS"files/playerPositions");
	string coordinate;

	for (int i = 0; i < 36; ++i)
	{
		filePositionsCpu >> coordinate;
		cpuHorse.positions [i].x = stoi (coordinate);

		filePositionsCpu >> coordinate;
		cpuHorse.positions [i].y = stoi (coordinate);

		filePositionsPlayer >> coordinate;
		playerHorse.positions [i].x = stoi (coordinate);

		filePositionsPlayer >> coordinate;
		playerHorse.positions [i].y = stoi (coordinate);
	}

	filePositionsCpu.close ();
	filePositionsPlayer.close ();

	cpuHorse.setPosition (cpuHorse.positions [0]);
	playerHorse.setPosition (playerHorse.positions [0]);

	auto & font = ResourceManager::getFont (ASSETS"fonts/My Big Heart Demo.ttf");

/*	auto & center = hud.getCenter ();
	auto & size = hud.getSize ();
	cpuCoordinates.setPosition (center.x - size.x * 0.5f, center.y + size.y * 0.5f - 150.f);
	cpuCoordinates.setCharacterSize (60U);
	cpuCoordinates.setFont (font);
	cpuCoordinates.setString ("X =\nY =");
	playerCoordinates.setString ("X =\nY =");
	playerCoordinates.setFont (font);
	playerCoordinates.setPosition (center.x - size.x * 0.5f, center.y - size.y * 0.5f);
	playerCoordinates.setCharacterSize (60U);
	viewOn = playerHorseType;*/

									////////// The HUD //////////
	hud.setCenter (game->window.getView ().getCenter ());
	hud.setSize (game->window.getView ().getSize ());
	hud.setParent (&game->window);

	auto readyButton = new Button (new Sprite (ResourceManager::getTexture (ASSETS"buttons/gameplay.png"), IntRect (0.f, 0.f, 254.f, 156.f)),
								   new Text ("Adelante", font, 60U), Vector2f (0.f, 0.f), &game->window);
	readyButton->scale (factor);
	readyButton->onClick.connect (bind (&Hipodrome::createScene, this, QuestionScene));

	#ifdef BYS_DESKTOP
	readyButton->onMouseEntered.connect (bind (&Button::scale, readyButton, Vector2f (1.2f * factor.x, 1.2f * factor.y)));
		readyButton->onMouseLeave.connect (bind (&Button::scale, readyButton, Vector2f (1.f * factor.x, 1.f * factor.y)));

	if (soundEffects)
		readyButton->onMouseEntered.connect (bind (&Sound::play, &bubbleSound));
	#endif
	hud.addWidget (readyButton, Vector2f (1200.f * factor.x, 700.f * factor.y));

	auto playerHorseFace = new Sprite (*playerHorseTexture, IntRect (0, 0, 78, 86));
	setOriginCenter (*playerHorseFace);
	playerHorseFace->setScale (factor);
	hud.addSprite (playerHorseFace, Vector2f (60.f * factor.x, 50.f * factor.y));

	auto cpuHorseFace = new Sprite (cpuHorseTexture, IntRect (0, 0, 78, 86));
	setOriginCenter (*cpuHorseFace);
	cpuHorseFace->setScale (factor);
	hud.addSprite (cpuHorseFace, Vector2f (60.f * factor.x, 175.f * factor.y));

	auto cpuHorsePositionText = new Text ("0", font, 70U);
	setOriginCenter (*cpuHorsePositionText);
	cpuHorsePositionText->setScale (factor);
	cpuHorse.stopRequest.connect (bind (&Hipodrome::setPositionText, this, cpuHorsePositionText, &cpuHorse));
	hud.addText (cpuHorsePositionText, Vector2f (150.f * factor.x, 175.f * factor.y));

	auto playerHorsePositionText = new Text ("0", font, 70U);
	setOriginCenter (*playerHorsePositionText);
	playerHorsePositionText->setScale (factor);
	playerHorse.stopRequest.connect (bind (&Hipodrome::setPositionText, this, playerHorsePositionText, &playerHorse));
	hud.addText (playerHorsePositionText, Vector2f (150.f * factor.x, 50.f * factor.y));

//	playerHorse.moveRequest.connect (bind (&HUD::setVisible, &hud, false));
	playerHorse.moveRequest.connect (bind (&Hipodrome::repositionHUD, this, playerHorseType));
//	playerHorse.stopRequest.connect (bind (&HUD::setVisible, &hud, true));
//	cpuHorse.moveRequest.connect (bind (&HUD::setVisible, &hud, false));
	cpuHorse.moveRequest.connect (bind (&Hipodrome::repositionHUD, this, CPU));
//	cpuHorse.stopRequest.connect (bind (&HUD::setVisible, &hud, true));

	hud.setCenter (playerHorse.getCurrentPosition ());

	if (music)
	{
		auto & backgroundMusic = ResourceManager::getMusic (ASSETS"/sounds/hipodrome.ogg");
		backgroundMusic.setLoop (true);
		backgroundMusic.play ();
	}
}

////////////////////////////////////////////////////
Hipodrome::~Hipodrome () {}

////////////////////////////////////////////////////
GameTask * Hipodrome::run ()
{
	while (this->running)
	{
		while (game->window.pollEvent (this->event))
		{
			hud.processEvents (event);
			switch (event.type)
			{
				case Event::Closed: break;

				case Event::KeyPressed:
					switch (event.key.code)
					{
						case Keyboard::Num1: cpuHorse.movePositions (1); break;
						case Keyboard::Num2: cpuHorse.movePositions (2); break;
						case Keyboard::Num3: cpuHorse.movePositions (3); break;
						case Keyboard::Num8: playerHorse.movePositions (1); break;
						case Keyboard::Num9: playerHorse.movePositions (2); break;
						case Keyboard::Num0: playerHorse.movePositions (3);
					}
			}
		}

		if (Keyboard::isKeyPressed (Keyboard::Escape))
		{
			this->task->setType (GameTask::TurnOff);
			this->close ();
		}

/*		if (Keyboard::isKeyPressed (Keyboard::W))
		{
			if (viewOn != playerHorse.getType ())
			{
				view.setCenter (playerHorse.getPosition ());
				viewOn = playerHorse.getType ();
				auto & center = view.getCenter ();
				auto & size = view.getSize ();
				cpuCoordinates.setPosition (center.x - size.x * 0.5f, center.y + size.y * 0.5f - 150.f);
				playerCoordinates.setPosition (center.x - size.x * 0.5f, center.y - size.y * 0.5f);
			}

			view.move (0.f, -5.f);
			cpuCoordinates.move (0.f, -5.f);
			playerCoordinates.move (0.f, -5.f);
			playerHorse.move (0.f, -5.f);
			auto & position = playerHorse.getPosition ();
			playerCoordinates.setString ("X = " + to_string (position.x) + "\nY = " + to_string (position.y));
		}

		if (Keyboard::isKeyPressed (Keyboard::S))
		{
			if (viewOn != playerHorse.getType ())
			{
				view.setCenter (playerHorse.getPosition ());
				viewOn = playerHorse.getType ();
				auto & center = view.getCenter ();
				auto & size = view.getSize ();
				cpuCoordinates.setPosition (center.x - size.x * 0.5f, center.y + size.y * 0.5f - 150.f);
				playerCoordinates.setPosition (center.x - size.x * 0.5f, center.y - size.y * 0.5f);
			}

			view.move (0.f, 5.f);
			cpuCoordinates.move (0.f, 5.f);
			playerCoordinates.move (0.f, 5.f);
			playerHorse.move (0.f, 5.f);
			auto & position = playerHorse.getPosition ();
			playerCoordinates.setString ("X = " + to_string (position.x) + "\nY = " + to_string (position.y));
		}

		if (Keyboard::isKeyPressed (Keyboard::A))
		{
			if (viewOn != playerHorse.getType ())
			{
				view.setCenter (playerHorse.getPosition ());
				viewOn = playerHorse.getType ();
				auto & center = view.getCenter ();
				auto & size = view.getSize ();
				cpuCoordinates.setPosition (center.x - size.x * 0.5f, center.y + size.y * 0.5f - 150.f);
				playerCoordinates.setPosition (center.x - size.x * 0.5f, center.y - size.y * 0.5f);
			}

			view.move (-5.f, 0.f);
			cpuCoordinates.move (-5.f, 0.f);
			playerCoordinates.move (-5.f, 0.f);
			playerHorse.move (-5.f, 0.f);
			auto & position = playerHorse.getPosition ();
			playerCoordinates.setString ("X = " + to_string (position.x) + "\nY = " + to_string (position.y));
		}

		if (Keyboard::isKeyPressed (Keyboard::D))
		{
			if (viewOn != playerHorse.getType ())
			{
				view.setCenter (playerHorse.getPosition ());
				viewOn = playerHorse.getType ();
				auto & center = view.getCenter ();
				auto & size = view.getSize ();
				cpuCoordinates.setPosition (center.x - size.x * 0.5f, center.y + size.y * 0.5f - 150.f);
				playerCoordinates.setPosition (center.x - size.x * 0.5f, center.y - size.y * 0.5f);
			}

			view.move (5.f, 0.f);
			cpuCoordinates.move (5.f, 0.f);
			playerCoordinates.move (5.f, 0.f);
			playerHorse.move (5.f, 0.f);
			auto & position = playerHorse.getPosition ();
			playerCoordinates.setString ("X = " + to_string (position.x) + "\nY = " + to_string (position.y));
		}

		if (Keyboard::isKeyPressed (Keyboard::I))
		{
			if (viewOn != Cpu)
			{
				view.setCenter (playerHorse.getPosition ());
				viewOn = playerHorse.getType ();
				auto & center = view.getCenter ();
				auto & size = view.getSize ();
				cpuCoordinates.setPosition (center.x - size.x * 0.5f, center.y + size.y * 0.5f - 150.f);
				playerCoordinates.setPosition (center.x - size.x * 0.5f, center.y - size.y * 0.5f);
			}

			view.move (0.f, -5.f);
			cpuCoordinates.move (0.f, -5.f);
			playerCoordinates.move (0.f, -5.f);
			cpuHorse.move (0.f, -5.f);
			auto & position = cpuHorse.getPosition ();
			cpuCoordinates.setString ("X = " + to_string (position.x) + "\nY = " + to_string (position.y));
		}

		if (Keyboard::isKeyPressed (Keyboard::K))
		{
			if (viewOn != Cpu)
			{
				view.setCenter (playerHorse.getPosition ());
				viewOn = playerHorse.getType ();
				auto & center = view.getCenter ();
				auto & size = view.getSize ();
				cpuCoordinates.setPosition (center.x - size.x * 0.5f, center.y + size.y * 0.5f - 150.f);
				playerCoordinates.setPosition (center.x - size.x * 0.5f, center.y - size.y * 0.5f);
			}

			view.move (0.f, 5.f);
			cpuCoordinates.move (0.f, 5.f);
			playerCoordinates.move (0.f, 5.f);
			cpuHorse.move (0.f, 5.f);
			auto & position = cpuHorse.getPosition ();
			cpuCoordinates.setString ("X = " + to_string (position.x) + "\nY = " + to_string (position.y));
		}

		if (Keyboard::isKeyPressed (Keyboard::J))
		{
			if (viewOn != Cpu)
			{
				view.setCenter (playerHorse.getPosition ());
				viewOn = playerHorse.getType ();
				auto & center = view.getCenter ();
				auto & size = view.getSize ();
				cpuCoordinates.setPosition (center.x - size.x * 0.5f, center.y + size.y * 0.5f - 150.f);
				playerCoordinates.setPosition (center.x - size.x * 0.5f, center.y - size.y * 0.5f);
			}

			view.move (-5.f, 0.f);
			cpuCoordinates.move (-5.f, 0.f);
			playerCoordinates.move (-5.f, 0.f);
			cpuHorse.move (-5.f, 0.f);
			auto & position = cpuHorse.getPosition ();
			cpuCoordinates.setString ("X = " + to_string (position.x) + "\nY = " + to_string (position.y));
		}

		if (Keyboard::isKeyPressed (Keyboard::L))
		{
			if (viewOn != Cpu)
			{
				view.setCenter (playerHorse.getPosition ());
				viewOn = playerHorse.getType ();
				auto & center = view.getCenter ();
				auto & size = view.getSize ();
				cpuCoordinates.setPosition (center.x - size.x * 0.5f, center.y + size.y * 0.5f - 150.f);
				playerCoordinates.setPosition (center.x - size.x * 0.5f, center.y - size.y * 0.5f);
			}

			view.move (5.f, 0.f);
			cpuCoordinates.move (5.f, 0.f);
			playerCoordinates.move (5.f, 0.f);
			cpuHorse.move (5.f, 0.f);
			auto & position = cpuHorse.getPosition ();
			cpuCoordinates.setString ("X = " + to_string (position.x) + "\nY = " + to_string (position.y));
		}

		if (Keyboard::isKeyPressed (Keyboard::F))
			playerHorse.setScale (-1.f, 1.f);
		if (Keyboard::isKeyPressed (Keyboard::R))
			playerHorse.setScale (1.f, 1.f);

		if (Keyboard::isKeyPressed (Keyboard::P))
			cpuHorse.setScale (-1.f, 1.f);
		if (Keyboard::isKeyPressed (Keyboard::O))
			cpuHorse.setScale (1.f, 1.f);*/

		if (hud.isMoving ())
			hud.update ();
		else if (cpuHorse.isMoving ())
				 cpuHorse.update (&hud);
		else if (playerHorse.isMoving ())
			 	 playerHorse.update (&hud);

		game->window.clear (Color (0, 219, 53, 255));
		game->window.setView (hud);

		game->window.draw (spriteBackground);
		game->window.draw (cpuHorse);
		game->window.draw (playerHorse);
//		game->window.draw (cpuCoordinates);
//		game->window.draw (playerCoordinates);
		hud.draw ();

		game->window.display ();
	}

	running = true;
	return this->task;
}

////////////////////////////////////////////////////
#include "QuestionMenu.hpp"
#include "GameOver.hpp"
void Hipodrome::createScene (MarathonScenes scene)
{
	switch (scene)
	{
		case QuestionScene:
			this->close ();
			task->setType (GameTask::InsertScene);
			task->setScene (new QuestionMenu (game, &cpuHorse, &playerHorse, subjects));
		break;

		case GameOverScene:
			this->close ();
			task->setType (GameTask::InsertScene);
			task->setScene (new GameOver (game, subjects, &playerHorse));
	}
}

////////////////////////////////////////////////////
void Hipodrome::repositionHUD (HorseType horseRequestingMove)
{
	if (horseRequestingMove != horseUnderHUD)
	{
		if (horseRequestingMove == CPU)
			hud.moveTo (cpuHorse.getCurrentPosition ());
		else
			hud.moveTo (playerHorse.getCurrentPosition ());

		horseUnderHUD = horseRequestingMove;
	}
}

////////////////////////////////////////////////////
void Hipodrome::setPositionText (Text *text, Horse *horse) { text->setString (to_string (horse->getIteratorPosition ())); }
